# Toy hyperdimensional modeling of crystal formation

This uses the methods described in: https://journals.aps.org/prb/pdf/10.1103/PhysRevB.99.054102 
to speed up the modelling of crystals. It can visualize 1 and 2 dimensional crystals, and can in principle model the particles in N dimensions.
I haven't found a robust way to visualize 3 or more dimensions, but trust me - the code will work for it.

