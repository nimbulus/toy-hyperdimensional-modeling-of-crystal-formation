#toy model of a crystal, using https://journals.aps.org/prb/pdf/10.1103/PhysRevB.99.054102
#Copyright 2019 (c) sylvester zhang
#GNU GPLv3 or later


import matplotlib.pyplot as plt
import numpy


radius_minimized=1.0
epsilon = 10.0



def LJpotential(position1,position2,charge1,charge2,epoch,numRealDims):
    extra_dims=position1.size - numRealDims
    zero = numpy.zeros((extra_dims,),dtype='float')
    #LJ potential + nuclear repulsion potential (for salts) + (epoch**2)*distancefromrealdims
    return epsilon*((radius_minimized/numpy.linalg.norm(position1-position2))**(12) - (2*(radius_minimized/numpy.linalg.norm(position1-position2))**(6))) + (40*charge1*charge2)/numpy.linalg.norm(position1-position2) + 0.0001*(epoch**1.5)*(numpy.linalg.norm(position1[numRealDims-1:]-zero))


def total_energy_1d(list_of_particles,epoch): #{'aaa':{'position':(1.3,0.0,0.0),'charge':1}}
    energy=0.0
    for particle in list_of_particles:
        particle = list_of_particles[particle]
        charge=particle['charge']
        xpos = particle['position'][0]
        for other_particle in list_of_particles:
            other_particle = list_of_particles[other_particle]
            othercharge=other_particle['charge']
            if other_particle['position'][0]==xpos:
                pass
            #lennard-jones potential
            energy=energy+LJpotential(xpos,other_particle['position'][0],charge,othercharge,epoch,1) #only 1 real dimension
    return energy

def total_coloumbic_energy_Nd(list_of_particles,epoch,numRealDims=2):
    energy=0.0
    for particle in list_of_particles:
        particleI = list_of_particles[particle]
        energy = energy+individual_energy(particleI, list_of_particles,epoch,numRealDims,[particle])       
    return energy

def individual_energy(particle, list_of_particles,epoch,numRealDims,ignoreID):
    energy = 0.0
    charge=particle['charge']
    position = numpy.array(particle['position'])
    for other_particle in list_of_particles:
        other_particleI = list_of_particles[other_particle]
        other_charge=other_particleI['charge']
        other_position=numpy.array(other_particleI['position'])
        if numpy.linalg.norm(other_position-position)>0.0 and other_particle not in ignoreID:
            #print(numpy.linalg.norm(other_position-position))
            energy=energy+LJpotential(position,other_position,charge,other_charge,epoch,numRealDims)
    return energy

def descend(list_of_particles,epoch,dims,step_size=0.1): 
    #implements a very simple (not gradient cuz I dont know how to use np.gradient) descent step
    #by niggling the positions of each particle one by one
    #Im sure there's a much better way to do it but i can't be arsed
    for particle in list_of_particles:
        #particle is here is the unique identifier of the particle
        particleI = list_of_particles[particle] #particle item
        charge = particleI['charge']
        cur_position = numpy.array(particleI['position'])
        move_vector = numpy.zeros((cur_position.size,),dtype='float')
        cur_energy = individual_energy(particleI,list_of_particles,epoch,dims,[particle])
        cur_dimension=0 # which element of the array we are on right now
        for dimension in cur_position:
            
            #print(cur_dimension)
            #tweak each dimension
            for updown in (-1,1):
                imvector = numpy.zeros((cur_position.size,),dtype='float') #temporary vector to move in
                imvector[cur_dimension]=updown*step_size #eg: (0,0,0,-0.1,0)
                newparticle = {'position': tuple(cur_position+imvector),'charge':charge}
                energy = individual_energy(newparticle,list_of_particles,epoch,dims,[particle])
#                print(newparticle)
#                print(particleI)
#                print(energy)
#                print(cur_energy)
#                print("======================================")
                if energy < cur_energy: #we've reduced our energy!
                    move_vector[cur_dimension]=updown*step_size*((cur_energy-energy)**0.1) #ready to export the move vector
                    #print(move_vector)
            #print('yo')
            cur_dimension = cur_dimension+1
        list_of_particles[particle]['position']=tuple(numpy.array(particleI['position'])+move_vector)


def generate_particles(number,dimensions,charge1,charge2,spread):
    portion = round(number*spread)
    list_of_particles = {}
    for i in range(number):
        if i < portion:
            list_of_particles.update({str(i):{'position':(numpy.random.rand(dimensions,)-0.5*numpy.ones(dimensions,))*10,'charge':charge1}})
        if i > portion:
            list_of_particles.update({str(i):{'position':(numpy.random.rand(dimensions,)-0.5*numpy.ones(dimensions,))*10,'charge':charge2}})
    return list_of_particles

def draw_particles(list_of_particles,dims):
    x = []
    y = []
    colors = []
    for i in list_of_particles:
        x.append(list_of_particles[i]['position'][0])
        if dims==1:
            y.append(0)#list_of_particles[i]['position'][1])
        else:
            y.append(list_of_particles[i]['position'][1])
        colors.append(list_of_particles[i]['charge'])
    plt.scatter(x,y, c=colors, s=radius_minimized*100, alpha=0.5)
    plt.show()



epoch=0#all these are just graphing stuff
def main_iterator(list_of_particles,run_epochs,dims):
    xaxis=[]
    step_energies=[]

    for i in range(run_epochs):
        global epoch
        print(epoch)
        descend(list_of_particles,epoch,dims,step_size=0.01)
        
        step_energies.append(total_coloumbic_energy_Nd(list_of_particles,epoch,numRealDims=dims))
        xaxis.append(epoch)
        epoch = epoch+1
    plt.xlabel("simulation epoch")
    plt.ylabel("Energy (arbitrary units)")
    plt.plot(xaxis,step_energies,'r')
    plt.show()
    draw_particles(list_of_particles,len(list_of_particles['1']['position']))
    return step_energies
